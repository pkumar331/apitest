/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apitest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author jsingh
 */
@Path("/aavaz")
public class TestAPI {
    @GET
    @Path("/getstatus")
    @Produces(MediaType.APPLICATION_JSON)
    public Object callStatus() {
        Object response = new Object();
        System.out.println("Inside method");
        return response;
    }
    @POST
    @Path("/changeState")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response changeState(CallingFSM callFsmReq) {
        callFsmReq.setPreviousState("Hello");
        callFsmReq.setNewState("World");
//        return Response.status(Response.Status.ACCEPTED).entity(callFsmReq)
////                        .header("Access-Control-Allow-Origin", "*")
////                        .header("Access-Control-Allow-Credentials", "true")
////                        .header("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers")
////                        .header("Content-Type,Authorization,Access-Control-Allow-Origin,X-Requested-With,accept,Origin,Access-Control-Request-Method,Access-Control-Request-Headers")
////                        .header("Access-Control-Max-Age", "1209600")
//                        .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD").build();
        return Response.status(Response.Status.OK).entity(callFsmReq).build();
    }
}
