/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apitest;

//import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jsingh
 */
//@XmlRootElement(name = "result")
public class CallingFSM {

    private String requestTime;
    private String provider;
    private String to;
    private String callUID;
    private String sipID;
    private String event;
    private String previousState;
    private String newState;
    private String error;
    private String responseTime;

    public String getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(String requestTime) {
        this.requestTime = requestTime;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getCallUID() {
        return callUID;
    }

    public void setCallUID(String callUID) {
        this.callUID = callUID;
    }

    public String getSipID() {
        return sipID;
    }

    public void setSipID(String sipID) {
        this.sipID = sipID;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getPreviousState() {
        return previousState;
    }

    public void setPreviousState(String previousState) {
        this.previousState = previousState;
    }

    public String getNewState() {
        return newState;
    }

    public void setNewState(String newState) {
        this.newState = newState;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(String responseTime) {
        this.responseTime = responseTime;
    }

    @Override
    public String toString() {
        return "CallingFSM{" + "requestTime=" + requestTime + ", provider=" + provider + ", to=" + to + ", callUID=" + callUID + ", sipID=" + sipID + ", event=" + event + ", previousState=" + previousState + ", newState=" + newState + ", error=" + error + ", responseTime=" + responseTime + '}';
    }

}
